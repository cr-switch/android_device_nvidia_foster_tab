# Inherit some common carbon stuff.
$(call inherit-product, vendor/carbon/config/common_tablet.mk)

# Inherit device configuration for jetson-tk1.
$(call inherit-product, device/nvidia/foster_tab/full_foster_tab.mk)

PRODUCT_NAME := carbon_foster_tab
PRODUCT_DEVICE := foster_tab
